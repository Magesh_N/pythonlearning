import math
import pytest


def test_square_root():
    assert 5 == math.sqrt(25)


@pytest.mark.custom
def test_power_of_value(setup_teardown):
    print("Inside TestMethod")
    assert 4 == math.pow(2, 2)


@pytest.mark.parametrize("input,output",[(1,2),(2,3),(3,4)])
def test_parametrization_method(input,output):
    assert input + 1 == output


@pytest.mark.xfail
def test_ignore_failure():
    assert 4 == 5


@pytest.mark.skip
def test_skip():
    print("This test will be skipped")
