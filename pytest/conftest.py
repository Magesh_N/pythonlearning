import pytest


@pytest.fixture()
def setup_teardown():
    print("Setup invoked")
    yield
    print("Teardown invoked")