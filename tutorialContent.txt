﻿⭐️ Contents ⭐                                 | Tutorial | Implementation
⌨️ (0:00) Introduction                        | Done     | Done 
⌨️ (1:45) Installing Python & PyCharm         | Done     | Done 
⌨️ (6:40) Setup & Hello World                 | Done     | Done 
⌨️ (10:23) Drawing a Shape                    | Done     | ---- 
⌨️ (15:06) Variables & Data Types             | Done     | Done 
⌨️ (27:03) Working With Strings               | Done     | Done 
⌨️ (38:18) Working With Numbers               | Done     | ---- 
⌨️ (48:26) Getting Input From Users           | Done     | Done 
⌨️ (52:37) Building a Basic Calculator        | ----     | ----
⌨️ (58:27) Mad Libs Game                      | ----     | ----         
⌨️ (1:03:10) Lists                            | Done     | Done 
⌨️ (1:10:44) List Functions                   | Done     | Done 
⌨️ (1:18:57) Tuples                           | Done     | Done 
⌨️ (1:24:15) Functions                        | Done     | Done
⌨️ (1:34:11) Return Statement                 | Done     | Done
⌨️ (1:40:06) If Statements                    | Done     | Done
⌨️ (1:54:07) If Statements & Comparisons      | Done     | Done
⌨️ (2:00:37) Building a better Calculator     |          |
⌨️ (2:07:17) Dictionaries                     |          |
⌨️ (2:14:13) While Loop                       | Done     | Done         
⌨️ (2:20:21) Building a Guessing Game         |          |
⌨️ (2:32:44) For Loops                        | Done     | Done         
⌨️ (2:41:20) Exponent Function                |          |
⌨️ (2:47:13) 2D Lists & Nested Loops          |          |
⌨️ (2:52:41) Building a Translator            |          |
⌨️ (3:00:18) Comments                         |          |
⌨️ (3:04:17) Try / Except                     |          |
⌨️ (3:12:41) Reading Files                    |          |
⌨️ (3:21:26) Writing to Files                 |          |
⌨️ (3:28:13) Modules & Pip                    |          |
⌨️ (3:43:56) Classes & Objects                |          |
⌨️ (3:57:37) Building a Multiple Choice Quiz  |          |
⌨️ (4:08:28) Object Functions                 |          |
⌨️ (4:12:37) Inheritance                      |          |
⌨️ (4:20:43) Python Interpreter               |          |