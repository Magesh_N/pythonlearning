#Strings in Python
#How to define a string and exploration on few of its properties
string_line_separator = "#########################"
string_name = "Magesh"
print("This is a sample string sequence of how strings is used in Python. The word " + string_name + " is stored in a variable.")
string_name = "Magesh N"
print("The same string is modified as " + string_name + " and stored in the same variable")
print(string_line_separator)
print("String Replace:- Replacing " + string_name + " to ")
string_name = string_name.replace("N", "Nagamani")
print(string_name)
print(string_line_separator)
print("String length:- ", len(string_name))
print(string_line_separator)
print("Upper case:-" + string_name.upper())
print("is_Upper:-" , string_name.upper().isupper())
print("Lower case:-" + string_name.lower())
print("is_Lower:-" , string_name.lower().islower())
print(string_line_separator)