#if  statement
print("if statement in python")
print("this is a game, you will be asked three question and your answers will be calculated using if statemets")
string_answer1 = input("Is Python OOP language?- Yes/No:-")
string_answer2 = input("Is Python file saved with .py extension?- Yes/No:-")
string_answer3 = input("Do Python language support if else statements?- Yes/No:-")

#calculate average score
ans1 = 0
ans2 = 0
ans3 = 0
if string_answer1.lower() == "yes" :
    ans1 = 10
if string_answer2.lower() == "yes" :
    ans2 = 10
if string_answer3.lower() == "yes" :
    ans3 = 10

avg = (ans1 + ans2 + ans3) / 3

print("Your score is " + str(avg) + " out off 10")

