#if else statement
print("if else statements in python")
print("this is a game, you will be asked three question and your answers will be calculated using if else statemets")

def verify_answer(answer = ""):
    if answer.lower() == "yes":
        return "That's Correct!"
    elif answer.lower() == "no":
        return "OH noooo! Incorrect answer"
    else:
        return "You must give your answer in Yes/No combo"

print(verify_answer(input("Is Python OOP language?- Yes/No:-")))
print(verify_answer(input("Is Python file saved with .py extension?- Yes/No:-")))
print(verify_answer(input("Do Python language support if else statements?- Yes/No:-")))

