#functions in python
print("To declare a function in python start with 'def' followed by function_name and end with ():\n ex: def sample_func():")
def sample_function():
    print("Inside the sample_function")

sample_function()

#function with return type
print("Function with return type")
def function_that_return_value():
    string_input = input("The string you type will be returned to the caller of the function:- ")
    return string_input

print("String given by the user was:- "+ function_that_return_value())

#function with params - param1 as string & param2 as integer
print("Function with params - param1 as string & param2 as integer")
def function_with_param(param1, param2):
    print("Parameter-1: " + param1)
    print("Parameter-2: " + str(param2))

function_with_param("Magesh", "26")

#function with optional parameters
print("Function with optional parameters")
def function_with_optional_parameter(name = "Magesh, Owner of this repo :-) "):
    print("The parameter provided to this method was: " + name)

print("Calling the function_with_optional_parameter with default param")
function_with_optional_parameter()

print("Calling the function_with_optional_parameter with custom param")
function_with_optional_parameter("Riya--- Ne M")