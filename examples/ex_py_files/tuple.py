#dive into the concept of Tuple
print("Tuple: A collection/DS which is immutable.\nA tuple in python can be created by adding elements within round braces ()")
groceries_tuple =  ("Cooking oil", "Sauce", "Breads", "Masala")
print("Grocery tuple: ")
print(groceries_tuple)

print("Printing by order")
print(groceries_tuple[0])
print(groceries_tuple[1])
print(groceries_tuple[2])

print("Printing by reverse order")
print(groceries_tuple[::-1])

print("Is element available in tuple")
print(groceries_tuple.index("Sauce"))
