#all about list in python
print("List: A collection/DS which is mutable.\nA list in python can be created by adding elements within square braces []")
groceries_list =  ["Cooking oil", "Sauce", "Breads", "Masala"]
print("Grocery list: ")
print(groceries_list)

print("Printing by order")
print(groceries_list[0])
print(groceries_list[1])
print(groceries_list[2])

print("Printing by reverse order")
print(groceries_list[::-1])

print("Printing by ascending order")
groceries_list.sort(reverse=False)
print(groceries_list)

print("Printing by decending order")
groceries_list.sort(reverse=True)
print(groceries_list)

copy_groceries_list = groceries_list.copy()

print("Copied list:")
print(copy_groceries_list)

print("Is element available in list")
print(copy_groceries_list.index("Sauce"))

print("Add element to list")
copy_groceries_list.append("Vegetables")
print(copy_groceries_list)

print("Merge two lists: ")
groceries_list.extend(copy_groceries_list)
print(groceries_list)

print("Remove from list: ")
groceries_list.remove("Sauce")
print(groceries_list)