import logging

from logging_examples.BaseLogger import BaseLogger


class TestLoggerCode(BaseLogger):
    def logger_check(self):
        # create a file handler
        file_handler = logging.FileHandler("logFile.log")

        # create formatter
        formatter = logging.Formatter("%(asctime)s : %(levelname)s : %(name)s : %(message)s")

        # set formatter to fileHandlerObj
        file_handler.setFormatter(formatter)

        # create logger object
        logger_obj = logging.getLogger(__name__)

        # add handler to logger
        logger_obj.addHandler(file_handler)

        # set logger level
        logger_obj.setLevel(logging.INFO)

        # print some logs
        logger_obj.debug("Debug message")
        logger_obj.info("Info message")
        logger_obj.warning("Warning message")
        logger_obj.error("Error message")
        logger_obj.critical("Critical message")

    def test_sample_test(self):
        logger = self.get_logger()
        logger.info("Inside test_sample_test method")
