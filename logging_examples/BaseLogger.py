import inspect
import logging


class BaseLogger:
    def get_logger(self):
        # create a file handler
        file_handler = logging.FileHandler("logFile.log")

        # create formatter
        formatter = logging.Formatter("%(asctime)s : %(levelname)s : %(name)s : %(message)s")

        # set formatter to fileHandlerObj
        file_handler.setFormatter(formatter)

        # get method calling name and assign it in a variable
        method_name = inspect.stack()[1][3]

        # create logger object
        logger_obj = logging.getLogger(method_name)

        # add handler to logger
        logger_obj.addHandler(file_handler)

        # set logger level
        logger_obj.setLevel(logging.INFO)

        return logger_obj
